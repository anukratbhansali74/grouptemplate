---
title: Budget
subtitle: How Much Does it Cost?
comments: false
---

## Budget

Summarise how your spent your budget and how the cost affects certain use cases. 

Are there different costs associated with different use cases? Are there any other considerations to be taken with regards to cost?

You should include the costs taken from your budget and the cost of your equipment.

Tables could be useful in presenting this data. An example table is shown below:

|  Item | Quantity | Cost Per Unit | Total |
|:-----:|:--------:|:-------------:|:-----:|
| thing |     2    |       £5      |  £10  |
| stuff |    45    |       £2      |  £90  |

## Example HTML Usage

<p style="text-align: center;"><strong>Using HTML</strong></p>
<p style="text-align: center;">You can also embed html to your site as needed.</p>
<p style="text-align: center;">Small example of the code</p>
<table style="margin-left: auto; margin-right: auto;"><caption>Table 1: Our costs during the project</caption>
<thead>
<tr>
<th align="center">Item</th>
<th align="center">Quantity</th>
<th align="center">Cost Per Unit</th>
<th align="center">Total</th>
</tr>
</thead>
<tbody>
<tr>
<td align="center">thing</td>
<td align="center">2</td>
<td align="center">&pound;5</td>
<td align="center">&pound;10</td>
</tr>
<tr>
<td align="center">stuff</td>
<td align="center">45</td>
<td align="center">&pound;2</td>
<td style="text-align: center;" align="center">&pound;90</td>
</tr>
</tbody>
</table>
